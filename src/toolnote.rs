use serde::Serialize;


/// This is a Rust struct that represents the JSON that custom analyzers need to output.
#[derive(Serialize,PartialEq,Debug)]
#[allow(non_snake_case)]
pub struct ToolNote {
    pub file: String,
    pub line: Option<isize>,
    pub column: Option<isize>,
    pub function: Option<String>,
    pub message: String,
    pub r#type: String,
}

#[allow(non_snake_case)]
impl ToolNote {
    pub fn new(file: String,
               line: Option<isize>,
               column: Option<isize>,
               function: Option<String>,
               message: String,
               r#type: String
               ) -> ToolNote
    {
        ToolNote {
            file,
            line,
            column,
            function,
            message,
            r#type
        }
    }
    
}

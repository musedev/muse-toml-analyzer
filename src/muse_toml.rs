use std::fmt::Display;
use crate::toolnote::ToolNote;
use toml::Value;

pub fn describe(val_map: toml::Value, line: isize, file: &str) -> Option<ToolNote> {
    // Get the key and value out of the toml value
    let (key,val) = val_map.as_table()?.iter().next()?;

    // Output an informational message to the user based on the key and value
    let message = match (key.as_str(),val) {
        // Validate both toml types and keys
        ("build",Value::String(s)) =>
            format!("The command you use to build your code is {}",s),

        ("setup",Value::String(s)) => 
            format!("{} gets run before muse analyzes your code",s),

        ("arguments",Value::Array(v)) => {
            format!("The arguments that will get passed to the build command are `{}`",format_array(v))
        },

        ("important",Value::Array(v)) => 
            format!("These are the only issues I will see: {}",format_array(v)),

        ("ignore",Value::Array(v)) => 
            format!("The issues I will ignore are {}",format_array(v)),

        ("tools",Value::Array(v)) => 
            format!("The tools I will run are {}",format_array(v)),

        ("verifiers",Value::Array(v)) => 
            format!("The tools I will run are {}. WARNING: \"verifiers\" is deprecated and should be replaced by \"tools\"",format_array(v)),

        ("customTools",Value::Array(v)) => 
            format!("The custom tools I will run are {}",format_array(v)),

        ("allow",Value::Array(v)) | ("whitelist",Value::Array(v)) => 
            format!("I will only analyze commits from these users: {}",format_array(v)),

        ("jdk11",&Value::Boolean(jdk11)) => 
            format!("I {} use jdk11", if jdk11 {"will"} else {"will not"}),

        ("androidVersion",Value::Integer(version)) => 
            format!("I will use version {} of the Android SDK", version),

        ("errorProneBugPatterns",Value::Array(v)) => 
            format!("I will use these ErrorProne bug patterns: {}",format_array(v)),

        (key,_) => 
            format!("Unused config value: {}",key)
    };
    Some(ToolNote::new(file.to_owned(),Some(line),None,None,message,"Info".to_owned()))
}

fn format_array<A: Display>(items: &[A]) -> String {
    let length = items.len();
    match length {
        0 => "".to_owned(),
        1 => format!("{}",items[0]),
        2 => format!("{} and {}", items[0], items[1]),
        n => {
            let heads = items[0..n-1].iter().map(|x| format!("{}",x)).collect::<Vec<String>>().join(", ");
            let tail = &items[n-1];
            format!("{}, and {}",heads,tail)
        }
    }
}

use crate::parse;

#[test]
fn full_file_parse() {
    // Try parsing our test muse.toml file
    let result = parse("src/tests/muse.toml","muse.toml").unwrap();
    // Check that the given filename is a relative path
    assert_eq!(result[0].file,"muse.toml");
    // We check the length of the results here because it's way easier
    assert!(result.len()==9);
}

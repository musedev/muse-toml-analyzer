use crate::toolnote::ToolNote;
use crate::muse_toml::describe;
use toml;

#[test]
fn single_describe() {
    let toml_line=r#"build = "make" "#;
    let toml_val = toml::from_str(toml_line).unwrap();
    let line = 42;
    let result = describe(toml_val,line,".muse.toml");
    let expected_result = ToolNote::new(
        ".muse.toml".to_owned(),
        Some(42),
        None,
        None,
        "The command you use to build your code is make".to_owned(),
        "Info".to_owned());
    assert_eq!(result,Some(expected_result))
}

#[test]
#[should_panic]
fn invalid_toml() {
    let toml_line=r#"build = bork "#;
    let _toml_val: toml::Value = toml::from_str(toml_line).unwrap();
}

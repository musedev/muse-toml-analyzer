mod toolnote;
mod muse_toml;
#[cfg(test)]
mod tests;

use std::env;
use std::fs::File;
use std::io;
use std::io::{BufReader,BufRead,Read};
use std::path::Path;
use serde_json;


use muse_toml::describe;

use toolnote::ToolNote;

fn main() {
    // Get all the arguments to this tool that were passed in by Muse
    let args: Vec<String> = env::args().collect();

    // Get the filename
    let filename = args.get(1).expect("Please pass a file name as the first argument to this program");

    // Get the path to the repository we're working with
    let path = args.get(1).expect("Please pass a file name as the first argument to this program");

    // NOTE: This causes breakage if it runs on a subdirectory, as Repository::open() expects you
    // to be at the root of a git directory. In subdirectories this is not the case. There is a
    // different version of open() in libgit that recurses up to find the closest .git directory
    // but there's actually no reason to bother. The code checkout already happens in the haskell
    // code right before this tool gets run.

    //// Get the branch we should be working with
    //let branch = args.get(2).expect("Please pass a git branch as the second argument to this program");
    //// And try switching to that branch before anything else
    //let repo = Repository::open(path).expect("Unable to open given directory as a git repository");

    //// Get the canonical name of the commit or branch
    //let rev = repo.revparse_single(branch).unwrap();

    //repo.checkout_tree(
    //    &rev,
    //    None
    //).expect("Unable to checkout specified git branch");

    //repo.checkout_tree(&rev,None).expect("Unable to move head");



    // Get the command we should run
    let command = args.get(3).expect("Please pass a command as the third argument to this program");


    // And finally, actually run the command given by Muse
    match command.as_str() {
        "version" => print!("{}",version()),
        "applicable" => print!("{}",applicable(&path)),
        "run" => {
            let result = run(&path);  

            // Print all the tool notes in JSON format after running the analysis
            match result {
                Ok(result) => println!("{}",serde_json::to_string(&result).unwrap()),

                // If we got an error then print that instead in JSON form
                Err(e) => { 
                    let error = ToolNote::new(filename.to_owned(),None,None,None,format!("{}",e),"Error".to_owned());
                    let serialized_error = serde_json::to_string(&vec![error]).unwrap();
                    println!("{}",serialized_error)
                }

            }

        },
        e => panic!("Invalid command: {}",e)

    }
}

// Get the version of the Muse API we're using
// The Bulk API is version 1
fn version() -> isize {
    1
}

// Check if we can run on the given repository and path
fn applicable(base_path: &str) -> bool {
    get_applicable_files(base_path).len() >= 1
}

// Actually run the analysis
fn run(base_path: &str) -> io::Result<Vec<ToolNote>> {
    let mut notes = vec![];
    for (file,filename) in get_applicable_files(base_path) {
        notes.append(&mut parse(file.as_str(),filename)?);       
    }
    Ok(notes)
}

// Find all potential files that we can run on
fn get_applicable_files(base_path: &str) -> Vec<(String,&'static str)> {
    let potential_files = [ ".muse/config", ".muse/config.toml", ".muse.toml" ];
    potential_files.iter()
                   .map(|file| (base_path.to_owned() + "/" + file,*file))
                   .filter(|(path,_name)| Path::new(path).exists())
                   .collect()

}

fn parse(path: &str,filename: &str) -> io::Result<Vec<ToolNote>> {
    // Open the corresponding file
    let file = File::open(path)?;
    let reader = BufReader::new(file);

    // Step 2: Read through the file, parsing it line by line
    // Intermediate parse results get fed into a buffer
    let mut buf = String::new();

    let mut toolnotes = Vec::new();

    // As soon as the buffer successfully serializes we create the relevant tool note and clear the
    // buffer
    //
    // Continue this until end of file
    for (line_number,line) in reader.lines().enumerate() {
        buf.push_str(&line?);
        if let Ok(val) = toml::from_str(&buf) {
            if let Some(toolnote) = describe(val,line_number as isize,filename) {
                toolnotes.push(toolnote);
            }
            buf = String::new();
        }
    }

    // The first loop assumes the toml file is actually correct. Now, try serializing the whole file
    // to check for syntax errors
    let file = File::open(path)?;
    let mut reader = BufReader::new(file);
    let mut buf = Vec::new();
    let _ = reader.read_to_end(&mut buf)?;
    let _: toml::Value = toml::from_slice(&buf)?;

    Ok(toolnotes)

}

## muse-toml-analyzer

This is an example custom tool that complies to the v1 Muse API. It supports
three commands:

### `muse-toml-analyzer <repo_path> <branch> version`

* Outputs the version of the API we're using, version 1.

### `muse-toml-analyzer <repo_path> <branch> applicable`

* Outputs whether or not our tool can run on the given repository. This just checks to
see if there's any Muse TOML files we can analyze.

### `muse-toml-analyzer <repo_path> <branch> run`
* Runs the analysis on the given repository. This returns a list of JSON-encoded
notes about the TOML files that the tool analyzed.
